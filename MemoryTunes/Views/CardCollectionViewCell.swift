import UIKit
import Kingfisher

final class CardCollectionViewCell: UICollectionViewCell {
  @IBOutlet weak var cardImageView: UIImageView!
  
  func configureCell(with cardState: MemoryCard) {
    let url = URL(string: cardState.imageUrl)
    cardImageView.kf.setImage(with: url)
    cardImageView.alpha = cardState.isAlreadyGuessed || cardState.isFlipped ? 1 : 0
  }
}
