import ReSwift

struct ChangeCategoryAction: Action {
  let categoryIndex: Int
}
