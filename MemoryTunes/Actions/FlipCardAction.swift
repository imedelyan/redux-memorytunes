import ReSwift

struct FlipCardAction: Action {
  let cardIndexToFlip: Int
}
